import { useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";

function format(value, pattern) {
  let i = 0;
  const v = value.toString();
  return pattern.replace(/#/g, _ => v[i++]);
}
const User = (props) => (
  <tr className="border-b transition-colors hover:bg-muted/50 data-[state=selected]:bg-muted">
    <td className="p-4 align-middle [&amp;:has([role=checkbox])]:pr-0">
      {props.user.name}
    </td>
    <td className="p-4 align-middle [&amp;:has([role=checkbox])]:pr-0">
      {format(props.user.password,'********')}
    </td>
    <td className="p-4 align-middle [&amp;:has([role=checkbox])]:pr-0">
      {props.user.profile}
    </td>
    <td className="p-4 align-middle [&amp;:has([role=checkbox])]:pr-0">
      <div className="flex gap-2">
        <Link
          className="inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-slate-100 h-9 rounded-md px-3"
          to={`/user/edit/${props.user._id}`}
        >
          Editar
        </Link>
        <button
          className="inline-flex items-center justify-center whitespace-nowrap text-sm font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-slate-100 hover:text-accent-foreground h-9 rounded-md px-3"
          color="red"
          type="button"
          onClick={() => {
            props.deleteItem(props.user._id);
          }}
        >
          Eliminar
        </button>
      </div>
    </td>
  </tr>
);

export default function UserList() {
  const [users, setRecords] = useState([]);

  // This method fetches the users from the database.
  useEffect(() => {
    async function getRecords() {
      const response = await fetch(`http://localhost:5050/user/`);
      if (!response.ok) {
        const message = `An error occurred: ${response.statusText}`;
        console.error(message);
        return;
      }
      const users = await response.json();
      setRecords(users);
    }
    getRecords();
    return;
  }, [users.length]);

  // This method will delete a user
  async function deleteItem(id) {

    await fetch(`http://localhost:5050/user/${id}`, {
      method: "DELETE",
    });
    const newRecords = users.filter((el) => el._id !== id);
    setRecords(newRecords);
  }

  // This method will map out the users on the table
  function itemList() {
    return users.map((user) => {
      return (
        <User
          user={user}
          deleteItem={() => deleteItem(user._id)}
          key={user._id}
        />
      );
    });
  }

  // This following section will display the table with the users of individuals.
  return (
    <>
      <h3 className="text-lg font-semibold p-4">Usuarios registrados</h3>
      <NavLink className="inline-flex items-center justify-center whitespace-nowrap text-md font-medium ring-offset-background transition-colors focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:pointer-events-none disabled:opacity-50 border border-input bg-background hover:bg-slate-100 h-9 rounded-md px-3"
        to="/user/create">
        Crear usuario
      </NavLink>
      <div className="border rounded-lg overflow-hidden">
        <div className="relative w-full overflow-auto">
          <table className="w-full caption-bottom text-sm">
            <thead className="[&amp;_tr]:border-b">
              <tr className="border-b transition-colors hover:bg-muted/50 data-[state=selected]:bg-muted">
                <th className="h-12 px-4 text-left align-middle font-medium text-muted-foreground [&amp;:has([role=checkbox])]:pr-0">
                  Nombre Completo
                </th>
                <th className="h-12 px-4 text-left align-middle font-medium text-muted-foreground [&amp;:has([role=checkbox])]:pr-0">
                  Contrase&ntilde;a
                </th>
                <th className="h-12 px-4 text-left align-middle font-medium text-muted-foreground [&amp;:has([role=checkbox])]:pr-0">
                  Perfil
                </th>
                <th className="h-12 px-4 text-left align-middle font-medium text-muted-foreground [&amp;:has([role=checkbox])]:pr-0">
                  Acciones
                </th>
              </tr>
            </thead>
            <tbody className="[&amp;_tr:last-child]:border-0">
              {itemList()}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
