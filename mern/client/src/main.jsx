import * as React from "react";
import * as ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import App from "./App";
import User from "./components/users/User";
import UserList from "./components/users/UserList";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        path: "/user",
        element: <UserList />,
      },
    ],
  },
  {
    path: "/user/create",
    element: <App />,
    children: [
      {
        path: "/user/create",
        element: <User />,
      },
    ],
  },
  {
    path: "/user/edit/:id",
    element: <App />,
    children: [
      {
        path: "/user/edit/:id",
        element: <User />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
